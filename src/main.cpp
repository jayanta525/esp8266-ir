/*

iBall Remote codes

12  Bass +  0xC
13  Bass -  0xD

20  Center +  0x14
21  Center -  0x15

72  Play/Pause  0x48
73  Previous  0x49
74  Vol - 0x4A
75  Next  0x4B
76  Vol + 0x4C

83  Channel  0x53

92  Source  0x5C
93  ON/OFF  0x5D
94  Reset 0x5E
95  Mute  0x5F

*/

#include <Arduino.h>
#include <ESP8266WiFi.h>
#include <ESPAsyncTCP.h>
#include <ESPAsyncWebServer.h>
#include <IRremote.h>
#include <ArduinoJson.h>
#include <ESP8266mDNS.h>
#include <WiFiUdp.h>
#include <ArduinoOTA.h>

#define IR_SEND_PIN 14 // D6 - D4/pin 2 is internal LED
#define IR_SEND_PIN_STRING "D5"
#define FEEDBACK_LED_IS_ACTIVE_LOW // The LED on my board is active LOW

AsyncWebServer server(80);

const char *ssid = "AP-EAP";
const char *password = "31060059";

const char *PARAM_MESSAGE = "cmd";

bool power = true;
bool channel = true;
int bass = 0;
uint8_t volume = 30;

uint16_t sAddress = 0x01;
uint8_t sRepeats = 0;

void notFound(AsyncWebServerRequest *request)
{
  request->send(404, "text/plain", "NA");
}

String retJSONcmd(String cmd, String val, bool isMax)
{
  StaticJsonDocument<200> jsonBuffer;
  jsonBuffer["confirm"] = "OK";
  jsonBuffer["command"] = cmd;
  jsonBuffer["value"] = val;
  jsonBuffer["max"] = isMax;

  String json;
  serializeJson(jsonBuffer, json);
  return json;
}

void setup()
{
  Serial.begin(115200);

  WiFi.mode(WIFI_STA);
  WiFi.begin(ssid, password);
  if (WiFi.waitForConnectResult() != WL_CONNECTED)
  {
    Serial.printf("WiFi Failed!\n");
    return;
  }

  Serial.print("IP Address: ");
  Serial.println(WiFi.localIP());

  server.on("/", HTTP_GET, [](AsyncWebServerRequest *request)
            {
              StaticJsonDocument<200> doc;

              doc["power"] = power;
              doc["channel"] = channel;
              doc["bass"] = bass;
              doc["volume"] = volume;

              String status;
              serializeJson(doc, status);
              request->send(200, "text/plain", status); });

  // Send a GET request to <IP>/get?message=<message>
  server.on("/get", HTTP_GET, [](AsyncWebServerRequest *request)
            {
              //char message;
              if (request->hasParam(PARAM_MESSAGE))
              {
                String message = request->getParam(PARAM_MESSAGE)->value();
                uint8_t swf = message.toInt();
                switch (swf)
                {
                case 1:
                  IrSender.sendNEC(sAddress, 0x5D, sRepeats);
                  power = !power;
                  request->send(200, "text/plain", "OK");
                  break;
                case 2:
                  if (bass < 10)
                  {
                    IrSender.sendNEC(sAddress, 0xC, sRepeats);
                    bass++;
                  }
                  request->send(200, "text/plain", retJSONcmd("bass", String(bass), bass == 10));
                  break;
                case 3:
                  if (bass > -10)
                  {
                    IrSender.sendNEC(sAddress, 0xD, sRepeats);
                    bass--;
                  }
                  request->send(200, "text/plain", retJSONcmd("bass", String(bass), bass == -10));
                  break;
                case 4:
                  if (volume < 100)
                  {
                    IrSender.sendNEC(sAddress, 0x4C, sRepeats);
                    volume++;
                  }
                  request->send(200, "text/plain", retJSONcmd("volume", String(volume), volume == 100));
                  break;
                case 5:
                  if (volume > 0)
                  {
                    IrSender.sendNEC(sAddress, 0x4A, sRepeats);
                    volume--;
                  }
                  request->send(200, "text/plain", retJSONcmd("volume", String(volume), volume == 0));
                  break;
                case 6:
                  IrSender.sendNEC(sAddress, 0x53, sRepeats);
                  channel = !channel;
                  request->send(200, "text/plain", "OK");
                  break;
                case 7:
                    IrSender.sendNEC(sAddress, 0x5C, sRepeats);
                    delay(1200);
                    IrSender.sendNEC(sAddress, 0x5C, sRepeats);
                    delay(1200);
                    IrSender.sendNEC(sAddress, 0x5C, sRepeats);
              
                  
                  request->send(200, "text/plain", "OK");
                  break;
                case 8:
                  IrSender.sendNEC(sAddress, 0x5E, sRepeats);
                  request->send(200, "text/plain", "OK");
                  break;
                default:
                  request->send(200, "text/plain", "NA");
                  break;
                }
              }
              else
                request->send(404, "text/plain", "NA"); });

  server.on("/custom", HTTP_GET, [](AsyncWebServerRequest *request)
            {
              //char message;
              if (request->hasParam(PARAM_MESSAGE))
              {
                String message = request->getParam(PARAM_MESSAGE)->value();
                IrSender.sendNEC(sAddress, message.toInt(), sRepeats);
                  request->send(200, "text/plain", "OK");
              }
              else
                request->send(404, "text/plain", "NA"); });

  server.on("/status", HTTP_GET, [](AsyncWebServerRequest *request)
            { request->send(200, "text/plain", String(millis())); });

  server.onNotFound(notFound);
  DefaultHeaders::Instance().addHeader("Access-Control-Allow-Origin", "*");
  server.begin();
  IrSender.begin(IR_SEND_PIN, ENABLE_LED_FEEDBACK); // Specify send pin and enable feedback LED at default feedback LED pin
  ArduinoOTA.onStart([]()
                     {
                       String type;
                       if (ArduinoOTA.getCommand() == U_FLASH)
                       {
                         type = "sketch";
                       }
                       else
                       { // U_FS
                         type = "filesystem";
                       }

                       // NOTE: if updating FS this would be the place to unmount FS using FS.end()
                       Serial.println("Start updating " + type); });
  ArduinoOTA.onEnd([]()
                   { Serial.println("\nEnd"); });
  ArduinoOTA.onProgress([](unsigned int progress, unsigned int total)
                        { Serial.printf("Progress: %u%%\r", (progress / (total / 100))); });
  ArduinoOTA.onError([](ota_error_t error)
                     {
                       Serial.printf("Error[%u]: ", error);
                       if (error == OTA_AUTH_ERROR)
                       {
                         Serial.println("Auth Failed");
                       }
                       else if (error == OTA_BEGIN_ERROR)
                       {
                         Serial.println("Begin Failed");
                       }
                       else if (error == OTA_CONNECT_ERROR)
                       {
                         Serial.println("Connect Failed");
                       }
                       else if (error == OTA_RECEIVE_ERROR)
                       {
                         Serial.println("Receive Failed");
                       }
                       else if (error == OTA_END_ERROR)
                       {
                         Serial.println("End Failed");
                       } });
  ArduinoOTA.begin();
}

void loop()
{
  ArduinoOTA.handle();
}
